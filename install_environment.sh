#!/bin/bash

set -e

# Manual de instalação: https://docs.nvidia.com/deeplearning/frameworks/install-tf-jetson-platform/index.html
# Manual de instalação:  https://docs.nvidia.com/deeplearning/frameworks/pdf/Install-TensorFlow-For-Jetson-TX2.pdf

echo -e "Versão utilizada: 1.13.1+nv19.5"
echo -e "Corresponde à versão: tf_gpu-1.13.1+nv19.5-py3 para o JetPack 4.2 no site [https://developer.nvidia.com/embedded/downloads#?search=tensorflow]"
echo -e "Link directo wheel Python: https://developer.download.nvidia.com/compute/redist/jp/v42/tensorflow-gpu/tensorflow_gpu-1.13.1+nv19.5-cp36-cp36m-linux_aarch64.whl"

VERSION="${1:-"1.13.1+nv19.5"}"

echo -e "Para especificar a versão utilizar o formato {versão tensorflow}+nv{versão da Nvidia}\nExemplo para Tensorflow 1.12.0 e versão Nvidia de Maio de 2019: 1.12.0+nv19.5"
echo -e "Consultar o site da Nvidia [https://developer.nvidia.com/embedded/downloads#?search=tensorflow]"

LOG_FILE="logs.txt"
ERROR_FILE="errors.txt"

echo -e "Instalar dependências no sistema (dev para os wheels)"
sudo apt-get install libhdf5-serial-dev hdf5-tools libhdf5-dev zlib1g-dev 'zip' libjpeg8-dev > "$LOG_FILE" 2> "$ERROR_FILE"

echo -e "Instalar o pip3"
sudo apt-get install python3-pip >> "$LOG_FILE" 2>> "$ERROR_FILE"

echo -e "Actualizar a versão do pip3"
sudo pip3 install -U pip >> "$LOG_FILE" 2>> "$ERROR_FILE"

echo -e "Instalar packages para o Tensorflow"
sudo pip3 install -U numpy grpcio absl-py py-cpuinfo psutil portpicker six mock requests gast h5py astor termcolor protobuf keras-applications keras-preprocessing wrapt google-pasta >> "$LOG_FILE" 2>> "$ERROR_FILE"

if [ "$1" == "" ]; then
	echo -e "Instalar versão mais recente do Tensorflow"
	sudo pip3 install --pre --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v42 tensorflow-gpu=="1.13.1+nv19.5" >> "$LOG_FILE" 2>> "$ERROR_FILE"
else
	echo -e "Instalar versão especificada do Tensorflow"
	sudo pip3 install --pre --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v42 tensorflow-gpu=="$1" >> "$LOG_FILE" 2>> "$ERROR_FILE"
fi

echo -e "Instalar venv para conseguir criar o ambiente virtual"
sudo apt install python3-venv >> "$LOG_FILE" 2>> "$ERROR_FILE"

echo -e "Criar ambiente virtual, tira partido dos packages do sistema (Pensar em instalar apenas os packages de desenvolvimento e tudo o resto em ambiente virtual)"
python3 -m venv --system-site-packages  .
