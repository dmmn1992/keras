from src.network import Network
from src.layers import Dense, Conv


class RegularStride5(Network):
    def __init__(self, name: str, folder_name: str = None, batch_size: int = 32,
                 save_folder: str = 'saved_models', measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        super(RegularStride5, self).__init__(dataset=name, name=folder_name, batch_size=batch_size, save_folder=save_folder,
                                      description={'name': 'RegularStride5'}, measure=measure,
                                      add_batch_size_to_name=add_batch_size_to_name)
        kernel_size = 5
        layers = [
            Conv(num_kernels=32, kernel_size=kernel_size, input_shape=self.dataset_train_shape[1:], activation='relu',
                 max_pooling_size=2, dropout_percentage=20),
            Conv(num_kernels=128, kernel_size=kernel_size, activation='relu',
                 max_pooling_size=2, dropout_percentage=20),
            Dense(num_units=256, requires_flatten=True, activation='relu')
        ]
        for layer in layers:
            self.add_layer(layer)
        self.add_output_layer()
