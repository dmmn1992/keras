from keras.datasets import cifar10, cifar100, mnist, fashion_mnist

AVAILABLE_DATASETS = {
    'cifar10': {'set': cifar10, 'num_classes': 10},
    'cifar100': {'set': cifar100, 'num_classes': 100},
    'mnist': {'set': mnist, 'num_classes': 10, 'greyscale': True},
    'fashion_mnist': {'set': fashion_mnist, 'num_classes': 10, 'greyscale': True}
}


def get_dataset(name: str) -> {}:
    return AVAILABLE_DATASETS.get(name)
