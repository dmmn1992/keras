from keras import backend as K
from src.transferable import Transferable
from src.simple import Simple
from src.stride5 import Stride5

allow_training = False

# Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=3, trainable_layers=allow_training).fit()

# Transferable('fashion_mnist', './saved_models/mnist/model_50.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_50.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_50.h5', num_layers=3, trainable_layers=allow_training).fit()

# Transferable('fashion_mnist', './saved_models/mnist/model_75.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_75.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_75.h5', num_layers=3, trainable_layers=allow_training).fit()

# Faltam
# Transferable('fashion_mnist', './saved_models/mnist/model_100.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_100.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_100.h5', num_layers=3, trainable_layers=allow_training).fit()

allow_training = True

# Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=3, trainable_layers=allow_training).fit()

# Transferable('fashion_mnist', './saved_models/mnist/model_50.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_50.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_50.h5', num_layers=3, trainable_layers=allow_training).fit()

# Transferable('fashion_mnist', './saved_models/mnist/model_75.h5', num_layers=1, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_75.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', 'src/saved_models/mnist/model_75.h5', num_layers=3, trainable_layers=allow_training).fit()

#  Faltam

# Transferable('fashion_mnist', './saved_models/mnist/model_100.h5', num_layers=2, trainable_layers=allow_training).fit()
# Transferable('fashion_mnist', './saved_models/mnist/model_100.h5', num_layers=3, trainable_layers=allow_training).fit()


# project = Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=1)
# project = Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=2)
# project.fit()
# K.clear_session()
# project = Transferable('fashion_mnist', './saved_models/mnist/model_25.h5', num_layers=3)
# project.fit()
# project = Stride5('fashion_mnist', 'fashion_mnist_5')
# project.fit()
# project = Stride5('fashion_mnist', 'fashion_mnist_5')
# project.fit()
project = Simple('mnist')
project.fit()
# project = Simple('fashion_mnist')
# project.fit()
