from __future__ import print_function

import pickle

import keras
import numpy
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras import losses
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import os

batch_size = 32
num_classes = 10
epochs = 100
data_augmentation = False
num_predictions = 20
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_bw_cifar10_trained_model.h5'


def load_bw_cifar10():
    with open(os.path.join(os.getcwd(), "bw_cifar10", "train"), "rb") as open_file:
        train = pickle.load(open_file)
        temp_x_train = train["data"]
        temp_y_train = train["labels"]
    with open(os.path.join(os.getcwd(), "bw_cifar10", "test"), "rb") as open_file:
        test = pickle.load(open_file)
        temp_x_test = test["data"]
        temp_y_test = test["labels"]
    return (temp_x_train, temp_y_train), (temp_x_test, temp_y_test)


# The data, split between train and test sets:
(x_train, y_train), (x_test, y_test) = load_bw_cifar10()
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

myModel = Sequential()
myModel.add(Conv2D(20, (5, 5), input_shape=x_test.shape[1:]))
myModel.add(MaxPooling2D((2, 2)))
myModel.add(Flatten())
myModel.add(Dense(10))
myModel.add(Activation('softmax'))

# optimizer = keras.optimizers.SGD(lr=0.05, momentum=0.001)
optimizer = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)

myModel.compile(loss=losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])

# Converte os dados para o formato indicado e coloca os valores no intervalo [0, 1]
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

myModel.fit(x_train, y_train,
            batch_size=batch_size,
            epochs=epochs,
            validation_data=(x_test, y_test),
            shuffle=True, verbose=2)

# Save model and weights
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
model_path = os.path.join(save_dir, model_name)
# Guarda tudo, incluíndo o otimizador
# Também possível guardar apenas os pesos se extrair o valor de cada camada
myModel.save(model_path)
print('Saved trained model at %s ' % model_path)

interesting_layers = myModel.layers[:-2]
for i, layer in interesting_layers:
    weights, biases = layer.get_weights()
    numpy.save(os.path.join(save_dir, f"cifar10_layer_{i}_weights"), weights)
    numpy.save(os.path.join(save_dir, f"cifar10_layer_{i}_biases"), biases)

# Score trained model.
scores = myModel.evaluate(x_test, y_test, verbose=1)
print('Test loss:', scores[0])
print('Test accuracy:', scores[1])

# if __name__ == u'__main__':

# Jovem:    PT50 0035 0399 0007 3807 5004 0
# Extracto: PT50 0035 0399 0007 3807 5004 0
