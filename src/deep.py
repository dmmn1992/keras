from src.network import Network
from src.layers import Dense, Conv


class Deep(Network):
    def __init__(self, name: str, folder_name: str = None, batch_size: int = 32,
                 save_folder: str = 'saved_models', measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        super(Deep, self).__init__(dataset=name, name=folder_name, batch_size=batch_size, save_folder=save_folder,
                                   measure=measure, add_batch_size_to_name=add_batch_size_to_name)
        layers = [
            Conv(num_kernels=128, kernel_size=5, input_shape=self.dataset_train_shape[1:], activation='relu'),
            Conv(num_kernels=64, kernel_size=5, activation='relu'),
            Conv(num_kernels=32, kernel_size=3, activation='relu', dropout_percentage=20),
            Conv(num_kernels=16, kernel_size=3, activation='relu', max_pooling_size=2, dropout_percentage=20),
            Dense(num_units=256, requires_flatten=True, activation='relu')
        ]
        for layer in layers:
            self.add_layer(layer)
        self.add_output_layer()
