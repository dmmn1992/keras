import argparse
from src.simple import Simple
from src.stride5 import Stride5
from src.deep import Deep
from src.transferable import Transferable


class TransferCommand(object):
    def __init__(self, command_name, archs, datasets, default_number_layers, default_batch_size,
                 default_save_folder):
        self.archs = archs
        self.parser = argparse.ArgumentParser(command_name)
        self.parser.add_argument('-arch', help='The desired architecture to test', type=str, required=True,
                                 choices=self.archs.keys())
        self.parser.add_argument('-dataset', help=f"The name of the dataset to use {','.join(datasets)}",
                                 type=str, required=True, choices=datasets)
        self.parser.add_argument('-modelPath', help='The relative path to the model to use as base',
                                 type=str,
                                 required=True)
        self.parser.add_argument('-numberLayers', help='The number of layers to transfer',
                                 type=int,
                                 required=False, default=default_number_layers)
        self.parser.add_argument('-transferedLearn', help='The transfered layers learn or not, values [yes, no]',
                                 type=str, choices=['yes', 'no'],
                                 required=False, default='no')
        self.parser.add_argument('-batchSize',
                                 help='The batch size to process results, on CPUs smaller values are better, GPUs '
                                      f'larger values are better, defaults to {default_batch_size}',
                                 type=int, required=False, default=default_batch_size)
        self.parser.add_argument('-saveFolder',
                                 help='The save folder to use, either a name or a relative path to the current working'
                                      f'directory, defaults to {default_save_folder}',
                                 type=str, required=False, default=default_save_folder)

    def execute(self):
        args = self.parser.parse_args()
        Transferable(self.archs[args.arch], dataset=args.dataset, model_path=args.modelPath, batch_size=args.batchSize,
                     trainable_layers=args.transferedLearn == 'yes', num_layers=args.numberLayers,
                     save_folder=args.saveFolder) \
            .fit()


ARCHS = {
    'simple': Simple,
    'stride5': Stride5,
    'deep': Deep
}

DATASETS = ['mnist', 'fashion_mnist']

DEFAULT_BATCH_SIZE = 128

DEFAULT_SAVE_FOLDER = 'save_models'

if __name__ == '__main__':
    parser = argparse.ArgumentParser('transfer')
    parser.add_argument('-arch', help='The desired architecture to test', type=str, required=True,
                        choices=ARCHS.keys())
    parser.add_argument('-dataset', help=f"The name of the dataset to use {','.join(DATASETS)}",
                        type=str, required=True, choices=DATASETS)
    parser.add_argument('-modelPath', help='The relative path to the model to use as base',
                        type=str,
                        required=True)
    parser.add_argument('-numberLayers', help='The number of layers to transfer',
                        type=int,
                        required=False, default=1)
    parser.add_argument('-transferedLearn', help='The transfered layers learn or not, values [yes, no]',
                        type=str, choices=['yes', 'no'],
                        required=False, default='no')
    parser.add_argument('-batchSize',
                        help='The batch size to process results, on CPUs smaller values are better, GPUs '
                             f'larger values are better, defaults to {DEFAULT_BATCH_SIZE}',
                        type=int, required=False, default=DEFAULT_BATCH_SIZE)
    parser.add_argument('-saveFolder',
                        help='The save folder to use, either a name or a relative path to the current working'
                             f'directory, defaults to {DEFAULT_SAVE_FOLDER}',
                        type=str, required=False, default=DEFAULT_SAVE_FOLDER)
    args = parser.parse_args()
    Transferable(ARCHS[args.arch], dataset=args.dataset, model_path=args.modelPath, batch_size=args.batchSize,
                 trainable_layers=args.transferedLearn == 'yes', num_layers=args.numberLayers,
                 save_folder=args.saveFolder) \
        .fit()
