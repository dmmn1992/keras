from src.cmds import COLOR_ARCHS, COLOR_DATASETS
from src.cmds.generate_graphs import GenerateGraphCommand

TIME_INDEX = 1

if __name__ == '__main__':
    command = GenerateGraphCommand(
        'timing_graph_2d',
        COLOR_ARCHS,
        COLOR_DATASETS,
        COLOR_DATASETS,
        TIME_INDEX,
        'timmings'
    )
    command.execute()
    exit(0)
