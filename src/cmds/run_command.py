from typing import Union, Dict
import argparse
from src.network import Network


class RunCommand(object):
    def __init__(self, command_name: str, archs: Dict[str, type(Network)], datasets_names: [str], batch_size: int = 32,
                 save_folder: Union[str, bytes] = 'save_models'):
        self.archs = archs
        self.parser = argparse.ArgumentParser(command_name)
        self.parser.add_argument('-arch', help='The desired architecture to test', type=str, required=True,
                                 choices=self.archs.keys())
        self.parser.add_argument('-dataset', help=f"The name of the dataset to use {','.join(datasets_names)}",
                                 type=str, required=True, choices=datasets_names)
        self.parser.add_argument('-folderName', help='The folder name to store results, will default to dataset name',
                                 type=str,
                                 required=False, default=None)
        self.parser.add_argument('-batchSize',
                                 help='The batch size to process results, on CPUs smaller values are better, GPUs '
                                      f'larger values are better, defaults to {batch_size}',
                                 type=int, required=False, default=batch_size)
        self.parser.add_argument('-saveFolder',
                                 help='The save folder to use, either a name or a relative path to the current working'
                                      f'directory, defaults to {save_folder}',
                                 type=str, required=False, default=save_folder)
        self.parser.add_argument('-measure', help='Set to True if measure per layer is required, may add overhead',
                                 type=str, required=False, default='no', choices=['yes', 'no'])

    def execute(self) -> None:
        args = self.parser.parse_args()
        arch = self.archs[args.arch]
        arch(name=args.dataset, folder_name=args.folderName, batch_size=args.batchSize, save_folder=args.saveFolder,
             measure=args.measure == 'yes').fit()
