from src.cmds import COLOR_ARCHS, COLOR_DATASETS
from src.cmds.generate_graphs import GenerateGraphCommand

ACCURACY_INDEX = 0

if __name__ == '__main__':
    command = GenerateGraphCommand(
        'accuracy_graph_2d',
        COLOR_ARCHS,
        COLOR_DATASETS,
        COLOR_DATASETS,
        ACCURACY_INDEX,
        'accuracy'
    )
    command.execute()
    exit(0)
