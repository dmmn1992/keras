from src.jetson import Good, Best, ConvHeavy, RegularConvHeavy, Deep as JetsonDeep, RegularDeep as JetsonRegularDeep

from src.simple import Simple
from src.stride5 import Stride5
from src.deep import Deep
from src.regular_simple import RegularSimple
from src.regular_stride5 import RegularStride5
from src.regular_deep import RegularDeep

BLACK_AND_WHITE_ARCHS = {
    'simple': Simple,
    'stride5': Stride5,
    'deep': Deep,
    'regular_simple': RegularSimple,
    'regular_stride5': RegularStride5,
    'regular_deep': RegularDeep
}

BLACK_AND_WHITE_DATASETS = ['mnist', 'fashion_mnist']

COLOR_ARCHS = {'good': Good, 'best': Best, 'conv_heavy': ConvHeavy, 'regular_conv_heavy': RegularConvHeavy,
               'deep': JetsonDeep, 'regular_deep': JetsonRegularDeep}

COLOR_DATASETS = ['cifar10', 'cifar100']
