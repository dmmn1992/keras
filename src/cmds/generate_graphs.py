import argparse
import os
from src import graphs


class GenerateGraphCommand(object):
    FUNCS_AND_NAMES = [(graphs.single_plot, ''),
                       (graphs.single_plot_zoomed_in, 'zoomed_in'),
                       (graphs.single_plot_difference, 'difference'),
                       (graphs.single_plot_difference_zoomed_in, 'difference_zoomed_in')
                       ]

    def __init__(self, command_name, archs: dict, original_datasets, datasets, index_access_values, filename,
                 percentage_values=False):
        self.values_index = index_access_values
        self.use_percentages_values = percentage_values
        self.filename = filename
        self.parser = argparse.ArgumentParser(command_name)
        self.parser.add_argument('-arch', help='The desired architecture to generate a graph', type=str, required=True,
                                 choices=archs.keys())
        self.parser.add_argument('-originalDataset',
                                 help=f"The name of the original dataset used to train the transferred model ({','.join(original_datasets)})",
                                 type=str, required=True, choices=original_datasets)
        self.parser.add_argument('-dataset',
                                 help=f"The name of the dataset used to train with transfer ({','.join(datasets)})",
                                 type=str, required=True, choices=datasets)
        self.parser.add_argument('-resultsOriginalFolder',
                                 help='Path to the folder where the original (without transfer) results are stored',
                                 type=str,
                                 required=False, default='save_models')
        self.parser.add_argument('-resultsTransferredFolder',
                                 help='Path to the folder where the transfer results are stored',
                                 type=str,
                                 required=False, default='transfered_models')
        self.parser.add_argument('-learning',
                                 help='Use the transferred models with learning or without (yes, no)',
                                 type=str,
                                 required=False, default='no', choices=['yes', 'no'])
        self.parser.add_argument('-saveFolder', help='The folder where to save the generated graphs', type=str,
                                 required=False, default='generated_graphs')
        self.parser.add_argument('-yLabel', help='The label on the y axis of the graph', required=False, type=str,
                                 default='')
        self.parser.add_argument('-xLabel', help='The label on the x axis of the graph', required=False, type=str,
                                 default='')
        self.parser.add_argument('-title', help='The title of the graph', required=False, type=str,
                                 default='')
        self.parser.add_argument('-grid', help='If a grid should be used on the graph (yes, no)', required=False,
                                 type=str,
                                 default='no', choices=['yes', 'no'])
        self.parser.add_argument('-usePercentage', help='If the difference graph should be in percentage (%)',
                                 required=False,
                                 type=str, default='yes', choices=['yes', 'no'])

    def __transfer_layers_legend_helper(self, number):
        if number == 1:
            return '1 Camada Transferida'
        else:
            return f'{number} Camadas Transferidas'

    def extract_values(self, original_tuple, transferred_tuple):
        values = [{'values': original_tuple[self.values_index]}]
        values.extend(map(lambda x: {'values': x[self.values_index]}, transferred_tuple))
        return values

    @staticmethod
    def __single_plot(func, values, title, legends, args):
        return func(values=values,
                    title=title,
                    grid=args.grid == 'yes',
                    ylabel=args.yLabel,
                    xlabel=args.xLabel,
                    legends=legends,
                    use_percentage=args.usePercentage == 'yes')

    def execute(self):
        args = self.parser.parse_args()
        did_learn = args.learning == 'yes'
        originals = graphs.find_original(arch_name=args.arch,
                                         dataset_name=args.dataset,
                                         path_to_folder=args.resultsOriginalFolder)
        transferred = graphs.find_transferred(arch_name=args.arch,
                                              original_dataset=args.originalDataset,
                                              dataset=args.dataset,
                                              learning=did_learn,
                                              path_to_folder=args.resultsTransferredFolder)
        for batch_size in transferred.keys():
            values = self.extract_values(originals[batch_size], transferred[batch_size])
            folder_path = os.path.join(os.getcwd(), args.saveFolder, args.arch, f"batch_{batch_size}", self.filename)
            os.makedirs(folder_path, exist_ok=True)
            legends = ['Original']
            legends.extend(
                [self.__transfer_layers_legend_helper(num + 1) for num in range(len(transferred[batch_size]))])
            title = f"Transferência de {args.originalDataset.replace('_', ' ').title()} para " \
                    f"{args.dataset.replace('_', ' ').title()} (batch {batch_size})" \
                    f"({'c' if did_learn else 's'}/ aprend.)"
            for func, name in self.FUNCS_AND_NAMES:
                figure_name = f"{args.originalDataset}_to_{args.dataset}" \
                              f"_{'' if did_learn else 'not_'}learning_{self.filename}{'_' + name if name else ''}.png"
                figure_path = os.path.join(folder_path, figure_name)
                fig = GenerateGraphCommand.__single_plot(
                    func=func,
                    values=values,
                    title=title,
                    legends=legends,
                    args=args)
                graphs.save_figure(path_to_figure=figure_path, fig=fig)
                graphs.close_figure(figure=fig)
        # # values = [{'values': originals[self.values_index]}]
        # # values.extend(map(lambda x: {'values': x[self.values_index]}, transferred))
        # # legends = ['Original']
        # # legends.extend([self.__transfer_layers_legend_helper(num + 1) for num in range(len(transferred))])
        # # title = f"Transferência de {args.originalDataset.replace('_', ' ').title()} para {args.dataset.replace('_', ' ').title()}"
        # # funcs_and_names = [(graphs.single_plot, ''), (graphs.single_plot_zoomed_in, 'zoomed_in'),
        # #                    (graphs.single_plot_difference, 'difference')]
        # folder_path = os.path.join(os.getcwd(), args.saveFolder, args.arch, self.filename)
        # os.makedirs(folder_path, exist_ok=True)
        # for func, name in funcs_and_names:
        #     figure_name = f"{args.arch}_{args.originalDataset}_to_{args.dataset}_{'' if did_learn else 'not_'}learning_{self.filename}{'_' + name if name else ''}.png"
        # figure_path = os.path.join(folder_path, figure_name)
        # fig = self.__single_plot(func=func, values=values, title=title, legends=legends, args=args)
        # graphs.save_figure(path_to_figure=figure_path, fig=fig)
        # graphs.close_figure(figure=fig)
