from src.cmds import COLOR_ARCHS, COLOR_DATASETS
from src.cmds.transfer_command import TransferCommand

DEFAULT_NUMBER_LAYERS = 1

DEFAULT_BATCH_SIZE = 128

DEFAULT_SAVE_FOLDER = 'save_models'

if __name__ == '__main__':
    command = TransferCommand('transfer_2d', COLOR_ARCHS, COLOR_DATASETS, DEFAULT_NUMBER_LAYERS, DEFAULT_BATCH_SIZE,
                              DEFAULT_SAVE_FOLDER)
    command.execute()
