from src.cmds import BLACK_AND_WHITE_ARCHS, BLACK_AND_WHITE_DATASETS
from src.cmds.run_command import RunCommand

DEFAULT_BATCH_SIZE = 32

if __name__ == '__main__':
    command = RunCommand('run_1d_test', archs=BLACK_AND_WHITE_ARCHS, datasets_names=BLACK_AND_WHITE_DATASETS,
                         batch_size=DEFAULT_BATCH_SIZE)
    command.execute()
    exit(0)
