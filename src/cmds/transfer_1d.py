from src.cmds import BLACK_AND_WHITE_ARCHS, BLACK_AND_WHITE_DATASETS
from src.cmds.transfer_command import TransferCommand

DEFAULT_NUMBER_LAYERS = 1

DEFAULT_BATCH_SIZE = 128

DEFAULT_SAVE_FOLDER = 'save_models'

if __name__ == '__main__':
    command = TransferCommand('transfer_1d', BLACK_AND_WHITE_ARCHS, BLACK_AND_WHITE_DATASETS,
                              DEFAULT_NUMBER_LAYERS, DEFAULT_BATCH_SIZE, DEFAULT_SAVE_FOLDER)
    command.execute()
    exit(0)
