from src.cmds import BLACK_AND_WHITE_ARCHS, BLACK_AND_WHITE_DATASETS
from src.cmds.generate_graphs import GenerateGraphCommand

TIME_INDEX = 1

if __name__ == '__main__':
    command = GenerateGraphCommand(
        'timing_graph_1d',
        BLACK_AND_WHITE_ARCHS,
        BLACK_AND_WHITE_DATASETS,
        BLACK_AND_WHITE_DATASETS,
        TIME_INDEX,
        'timmings'
    )
    command.execute()
    exit(0)
