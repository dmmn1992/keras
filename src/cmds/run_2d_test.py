from src.cmds import COLOR_ARCHS, COLOR_DATASETS
from src.cmds.run_command import RunCommand

DEFAULT_BATCH_SIZE = 128

if __name__ == '__main__':
    command = RunCommand('run_2d_test', archs=COLOR_ARCHS, datasets_names=COLOR_DATASETS,
                         batch_size=DEFAULT_BATCH_SIZE)
    command.execute()
