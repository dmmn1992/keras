from src.cmds import BLACK_AND_WHITE_ARCHS, BLACK_AND_WHITE_DATASETS
from src.cmds.generate_graphs import GenerateGraphCommand

ACCURACY_INDEX = 0

if __name__ == '__main__':
    command = GenerateGraphCommand(
        'accuracy_graph_1d',
        BLACK_AND_WHITE_ARCHS,
        BLACK_AND_WHITE_DATASETS,
        BLACK_AND_WHITE_DATASETS,
        ACCURACY_INDEX,
        'accuracy'
    )
    command.execute()
    exit(0)
