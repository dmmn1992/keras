from keras.layers import Dense as KerasDense, Activation, Flatten, Dropout

from src.layers.layer import Layer


class Dense(Layer):
    def __init__(self, num_units: int = 256, requires_flatten: bool = False, activation: str = 'relu',
                 dropout_percentage: float = 0.0, kernel_constraint=None) -> None:
        super().__init__({'type': 'Dense', 'number_units': num_units, 'flattened': requires_flatten,
                          'dropout_percentage': dropout_percentage, 'activation': activation})
        self.num_units = num_units
        self.requires_flatten = requires_flatten
        self.activation = activation
        self.dropout_percentage = dropout_percentage
        self.kernel_constraint = kernel_constraint

    def _generate(self):
        if self.requires_flatten:
            self._add_keras_layer(Flatten())
        self._add_keras_layer(KerasDense(self.num_units, use_bias=False, kernel_constraint=self.kernel_constraint))
        self._add_keras_layer(Activation(self.activation))
        if self.dropout_percentage > 0:
            self._add_keras_layer(Dropout(self.dropout_percentage / 100))
