from keras.layers import Activation, Dropout
from keras.layers import Conv2D, MaxPooling2D

from src.layers.layer import Layer


class Conv(Layer):
    def __init__(self, num_kernels: int = 128, kernel_size: int = 3, input_shape=None, activation: str = 'relu',
                 max_pooling_size: int = None, dropout_percentage: float = 0.0, strides: int = 1) -> None:
        super().__init__({'type': 'Conv1D', 'number_kernels': num_kernels, 'kernel_size': kernel_size,
                          'strides': strides, 'activation': activation})
        self.num_kernels = num_kernels
        self.kernel_size = kernel_size
        self.input_shape = input_shape
        self.activation = activation
        self.max_pooling_size = max_pooling_size
        self.dropout_percentage = dropout_percentage
        self.strides = strides

    def _generate(self):
        if self.input_shape:
            self._add_keras_layer(
                Conv2D(self.num_kernels, self.kernel_size, strides=self.strides, input_shape=self.input_shape))
            self._add_to_report('input_shape', self.input_shape)
        else:
            self._add_keras_layer(Conv2D(self.num_kernels, self.kernel_size, strides=self.strides))
        self._add_keras_layer(Activation(self.activation))
        if self.max_pooling_size:
            self._add_keras_layer(MaxPooling2D(pool_size=self.max_pooling_size))
            self._add_to_report('max_pooling_size', self.max_pooling_size)
        if self.dropout_percentage > 0:
            self._add_keras_layer(Dropout(self.dropout_percentage / 100.0))
            self._add_to_report('dropout_percentage', self.dropout_percentage)
