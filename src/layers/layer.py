from src.layers.keras_layer_proxy import KerasLayerProxy


class Layer(object):
    def __init__(self, layer_description=None):
        if layer_description is None:
            layer_description = {}
        self.__internal_layers = []
        self.__report = layer_description
        self.__generated = False
        self.__wrapped = False

    @property
    def keras_layers(self):
        self.generate()
        return self.__internal_layers

    @property
    def report(self):
        self.generate()
        if self.__wrapped:
            self.__report['timing_info'] = [layer.timing_info for layer in self.__internal_layers]
        return self.__report

    def active_measure(self):
        if not self.__wrapped:
            self.generate()
            self.__internal_layers = [KerasLayerProxy(layer) for layer in self.__internal_layers]
            self.__wrapped = True

    def _add_keras_layer(self, internal_layer):
        self.__internal_layers.append(internal_layer)

    def _add_to_report(self, key, data):
        self.__report[key] = data

    def _generate(self):
        raise NotImplementedError()

    def generate(self):
        if not self.__generated:
            self._generate()
            self.__generated = True
