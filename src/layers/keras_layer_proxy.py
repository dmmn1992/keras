import inspect
import types

from time import time
from numpy import mean, amax, amin, median

from keras.layers import Layer


class KerasLayerProxy(Layer):
    MAX_INVOCATIONS_WITHOUT_CACHE = 2

    def __init__(self, original, **kwargs):
        super().__init__(**kwargs)
        # for name, value in inspect.getmembers(original):
        #     if name not in ['call', 'build', 'compute_output_shape', 'losses'] and value is not None:
        #         setattr(self, name, value)
        self.__times = []
        self.__current_start = None
        self.__call__ = orig
        self.__original = original
        self.__cache_attrs = {}
        self.__timing_info = None
        self.name = original.name

    @property
    def timing_info(self):
        if self.__timing_info is None:
            self.__timing_info = {
                'keras_name': type(self.__original).__name__,
                'all_values': self.__times,
                'mean': mean(self.__times),
                'max': amax(self.__times),
                'min': amin(self.__times),
                'median': median(self.__times),
                'activations': len(self.__times)
            }
        return self.__timing_info

    # @property
    # def __class__(self):
    #     return self.__original.__class__

    def call(self, inputs, **kwargs):
        self.__current_start = time()
        result = self.__original.call(inputs, **kwargs)
        self.__times.append(time() - self.__current_start)
        return result

    def build(self, input_shape):
        self.__original.build(input_shape)

    # def __call__(self, *args, **kwargs):
    #     pass

    def compute_output_shape(self, input_shape):
        return self.__original.compute_output_shape(input_shape)

    def __getattr__(self, name):
        if name in self.__cache_attrs:
            self.__cache_attrs[name] += 1
        else:
            self.__cache_attrs[name] = 1
        attr = getattr(self.__original, name)
        type_attr = type(attr)
        if (
                isinstance(attr, types.FunctionType) and
                self.__cache_attrs[name] > KerasLayerProxy.MAX_INVOCATIONS_WITHOUT_CACHE or
                not isinstance(attr, types.FunctionType)
        ):
            setattr(self, name, attr)
        return attr
