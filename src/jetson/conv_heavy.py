from src.network import Network
from src.jetson.layers import Conv
from src.layers.dense import Dense


class ConvHeavy(Network):
    def __init__(self, name: str, folder_name: str = None, batch_size: int = 256,
                 save_folder: str = 'saved_models', measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        super(ConvHeavy, self).__init__(dataset=name, name=folder_name, batch_size=batch_size, save_folder=save_folder,
                                        description={'name': 'ConvHeavyJetson'}, measure=measure,
                                        add_batch_size_to_name=add_batch_size_to_name)
        layers = [
            Conv(num_kernels=32, kernel_size=3, input_shape=self.dataset_train_shape[1:], padding='same',
                 activation='relu'),
            Conv(num_kernels=32, kernel_size=3, max_pooling_size=2, dropout_percentage=25,
                 activation='relu'),
            Conv(num_kernels=64, kernel_size=3, padding='same', activation='relu'),
            Conv(num_kernels=64, kernel_size=3, strides=2, max_pooling_size=2, dropout_percentage=25,
                 activation='relu'),
            Dense(num_units=512, requires_flatten=True, dropout_percentage=50, activation='relu')
        ]
        for layer in layers:
            self.add_layer(layer)
        self.add_output_layer()
