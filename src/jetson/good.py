from keras import losses
from keras.optimizers import SGD

from src.network import Network
from src.jetson.layers import Conv
from src.layers import Dense
from keras.constraints import maxnorm


class Good(Network):
    def __init__(self, name: str, folder_name: str = None, batch_size: int = 32,
                 save_folder: str = 'saved_models', measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        super(Good, self).__init__(dataset=name, name=folder_name, batch_size=batch_size,
                                   save_folder=save_folder, description={'name': 'GoodJetson'},
                                   measure=measure, add_batch_size_to_name=add_batch_size_to_name)
        layers = [
            Conv(num_kernels=32, kernel_size=3, input_shape=self.dataset_train_shape[1:],
                 padding='same', activation='relu', dropout_percentage=20),
            Conv(num_kernels=32, kernel_size=3, padding='same', activation='relu', max_pooling_size=2,
                 dropout_percentage=20),
            Dense(num_units=512, dropout_percentage=20, requires_flatten=True, kernel_constraint=maxnorm(3)),
        ]
        for layer in layers:
            self.add_layer(layer)
        self.add_output_layer()

    def fit(self, starting_epoch: int = 0, loss=losses.categorical_crossentropy,
            optimizer=SGD(lr=0.01, momentum=0.9, decay=1e-4, nesterov=False), metrics=None, callbacks=None):
        return super().fit(starting_epoch=starting_epoch,
                           optimizer=optimizer, metrics=metrics, callbacks=callbacks)
