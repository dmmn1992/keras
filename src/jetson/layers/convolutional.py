from typing import Any

from keras.layers import Activation, Dropout, Conv2D, MaxPooling2D, BatchNormalization
from keras import regularizers

from src.layers.layer import Layer


class Conv(Layer):
    def __init__(self, num_kernels: int = 128, kernel_size: int = 3, input_shape=None, activation: str = 'relu',
                 max_pooling_size: int = None, dropout_percentage: float = 0.0, strides: int = 1,
                 padding: str = 'valid', kernel_regularizer: Any = None, use_batch_normalization: bool = False) -> None:
        super().__init__({'type': 'Conv2D', 'number_kernels': num_kernels, 'kernel_size': kernel_size,
                          'strides': (strides, strides), 'activation': activation, 'padding': padding})
        self.num_kernels = num_kernels
        self.kernel_size = kernel_size
        self.input_shape = input_shape
        self.activation = activation
        self.max_pooling_size = (max_pooling_size, max_pooling_size) if max_pooling_size else None
        self.dropout_percentage = dropout_percentage
        self.strides = (strides, strides)
        self.padding = padding
        self.kernel_regularizer = kernel_regularizer
        self.use_batch_normalization = use_batch_normalization

    def _generate(self):
        if self.input_shape:
            self._add_keras_layer(Conv2D(self.num_kernels, kernel_size=self.kernel_size,
                                         strides=self.strides, padding=self.padding,
                                         input_shape=self.input_shape))
            self._add_to_report('input_shape', self.input_shape)
        else:
            self._add_keras_layer(
                Conv2D(self.num_kernels, kernel_size=self.kernel_size, padding=self.padding,
                       strides=self.strides, kernel_regularizer=self.kernel_regularizer))
        self._add_keras_layer(Activation(self.activation))
        if self.use_batch_normalization:
            self._add_keras_layer(BatchNormalization())
            self._add_to_report('use_batch_normalization', self.use_batch_normalization)
        if self.max_pooling_size:
            self._add_keras_layer(MaxPooling2D(pool_size=self.max_pooling_size))
            self._add_to_report('max_pooling_size', self.max_pooling_size)
        if self.dropout_percentage > 0:
            self._add_keras_layer(Dropout(self.dropout_percentage / 100.0))
            self._add_to_report('dropout_percentage', self.dropout_percentage)
