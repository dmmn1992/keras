from .conv_heavy import ConvHeavy
from .deep import Deep
from .regular_deep import RegularDeep
from .regular_conv_heavy import RegularConvHeavy
from .good import Good
from .best import Best
