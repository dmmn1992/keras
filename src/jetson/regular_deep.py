from src.network import Network
from src.jetson.layers import Conv
from src.layers.dense import Dense


class RegularDeep(Network):
    def __init__(self, name: str, folder_name: str = None, batch_size: int = 32,
                 save_folder: str = 'saved_models', measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        super(RegularDeep, self).__init__(dataset=name, name=folder_name, batch_size=batch_size, save_folder=save_folder,
                                   description={'name': 'RegularDeepJetson'}, measure=measure,
                                   add_batch_size_to_name=add_batch_size_to_name)
        layers = [
            Conv(num_kernels=32, kernel_size=3, input_shape=self.dataset_train_shape[1:], padding='same',
                 activation='relu'),
            Conv(num_kernels=64, kernel_size=3, activation='relu', max_pooling_size=2, dropout_percentage=25),
            Conv(num_kernels=128, kernel_size=3, padding='same', activation='relu'),
            Conv(num_kernels=256, kernel_size=3, activation='relu', max_pooling_size=2, dropout_percentage=25),
            Dense(num_units=512, requires_flatten=True, activation='relu', dropout_percentage=50)
        ]
        for layer in layers:
            self.add_layer(layer)
        self.add_output_layer()
