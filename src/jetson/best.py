from typing import Tuple, Any

from keras import losses, optimizers, regularizers
from keras.callbacks import LearningRateScheduler
from keras.utils import np_utils
from numpy.core.multiarray import ndarray

from src.debug import DEBUG
from src.network import Network
from src.jetson.layers import Conv
from src.layers import Dense as MyDense
from keras.constraints import maxnorm

import numpy as np


class Best(Network):
    def __init__(self, name: str, folder_name: str = None, batch_size: int = 32,
                 save_folder: str = 'saved_models', measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        super(Best, self).__init__(dataset=name, name=folder_name, batch_size=batch_size,
                                   save_folder=save_folder, description={'name': 'GoodJetson'},
                                   measure=measure, add_batch_size_to_name=add_batch_size_to_name)
        weight_decay = 1e-4
        layers = [
            Conv(num_kernels=32, kernel_size=3, input_shape=self.dataset_train_shape[1:],
                 padding='same', kernel_regularizer=regularizers.l2(weight_decay), activation='elu',
                 use_batch_normalization=True),
            Conv(num_kernels=32, kernel_size=3, padding='same',
                 kernel_regularizer=regularizers.l2(weight_decay), activation='elu',
                 use_batch_normalization=True, max_pooling_size=2, dropout_percentage=20),
            Conv(num_kernels=64, kernel_size=3, padding='same',
                 kernel_regularizer=regularizers.l2(weight_decay), activation='elu',
                 use_batch_normalization=True),
            Conv(num_kernels=64, kernel_size=3, padding='same',
                 kernel_regularizer=regularizers.l2(weight_decay), activation='elu',
                 use_batch_normalization=True, max_pooling_size=2, dropout_percentage=30),
            Conv(num_kernels=128, kernel_size=3, padding='same',
                 kernel_regularizer=regularizers.l2(weight_decay), activation='elu',
                 use_batch_normalization=True),
            Conv(num_kernels=128, kernel_size=3, padding='same',
                 kernel_regularizer=regularizers.l2(weight_decay), activation='elu',
                 use_batch_normalization=True, max_pooling_size=2, dropout_percentage=40),
        ]
        for layer in layers:
            self.add_layer(layer)
        self.add_output_layer(requiresFlatten=True)

    def _modify_dataset(self, data: Tuple[Tuple[ndarray, ndarray], Tuple[Any, ndarray]]) -> \
            Tuple[Tuple[ndarray, ndarray], Tuple[Any, ndarray]]:
        (x_train, y_train), (x_test, y_test) = data
        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        mean = np.mean(x_train, axis=(0, 1, 2, 3))
        std = np.std(x_train, axis=(0, 1, 2, 3))
        x_train = (x_train - mean) / (std + 1e-7)
        x_test = (x_test - mean) / (std + 1e-7)
        y_train = np_utils.to_categorical(y_train, self.num_classes)
        y_test = np_utils.to_categorical(y_test, self.num_classes)
        return (x_train, y_train), (x_test, y_test)

    @staticmethod
    def learning_rate_scheduler(epoch):
        if epoch > 75:
            return 0.0005
        if epoch > 100:
            return 0.0003
        return 0.001

    def fit(self, starting_epoch: int = 0, loss=losses.categorical_crossentropy,
            optimizer=optimizers.rmsprop(lr=0.001, decay=1e-6), metrics=None, callbacks=None):
        return super(Best, self).fit(starting_epoch=starting_epoch,
                                     optimizer=optimizer, metrics=metrics,
                                     callbacks=[LearningRateScheduler(schedule=Best.learning_rate_scheduler,
                                                                      verbose=1 if DEBUG == 1 else 0)])
