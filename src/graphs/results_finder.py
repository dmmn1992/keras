import json
import re
from os import getcwd, listdir
from os.path import join


def convert_tuple_to_dict(results_tuple):
    result_map = {}
    for index, value in enumerate(results_tuple[0]):
        result_map[value] = results_tuple[1][index]
    return result_map


def folder_for_pattern(path_to_folder: str, pattern: str):
    return sorted([join(path_to_folder, f) for f in listdir(path_to_folder) if
                   re.match(pattern=pattern, string=f, flags=re.IGNORECASE)])


def folders_for_pattern_with_batches(path_to_folder: str, pattern: str) -> ([int], [[float], [float]]):
    folders = folder_for_pattern(path_to_folder, pattern)
    batches = list(map(lambda x: int(x[x.rfind('_') + 1:]), folders))
    return batches, folders


def extract_results(path: str, evaluation_key: str = 'acc', times_key: str = 'times') -> ([float], [float]):
    with open(join(path, 'model_evaluation.json'), 'r') as fp:
        evaluations = json.load(fp)
    with open(join(path, 'model_time_info.json'), 'r') as fp:
        times = json.load(fp)
    return evaluations[evaluation_key], times[times_key]


def extract_result_from_folders(paths_to_folder: [str]) -> [[float], [float]]:
    return list(map(extract_results, paths_to_folder))


def find_original(arch_name: str, dataset_name: str, path_to_folder: str = 'save_models') -> (
        [int], [[float], [float]]):
    batches, path_to_results = folders_for_pattern_with_batches(join(getcwd(), path_to_folder),
                                                                f'{arch_name}_{dataset_name}_*')
    return convert_tuple_to_dict((batches, extract_result_from_folders(path_to_results)))


def find_transferred(arch_name: str, original_dataset: str, dataset: str, learning: bool,
                     path_to_folder: str = 'transfered_models', batch_folder_pattern: str = 'batch_[0-9]{1,3}') -> [
    ([float], [float])]:
    path_to_results_folder = join(getcwd(), path_to_folder, arch_name, f"{original_dataset}_to_{dataset}")
    batches, folders = folders_for_pattern_with_batches(path_to_results_folder, batch_folder_pattern)
    transferred_pattern = f"epoch_[0-9]{{1,4}}-[0-9]{{1,4}}_layers_{'' if learning else 'not_'}learning"
    folders = list(map(lambda x: folder_for_pattern(x, transferred_pattern), folders))
    return convert_tuple_to_dict((batches, [extract_result_from_folders(batch_folders) for batch_folders in folders]))


if __name__ == '__main__':
    results = find_transferred('simple', 'mnist', 'fashion_mnist', False)
    print(results)
