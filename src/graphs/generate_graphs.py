import numpy as np
import matplotlib.pyplot as plt
from functools import reduce

SMALL_SIZE = 28
MEDIUM_SIZE = 30
LARGE_SIZE = 34

plt.rc('font', size=MEDIUM_SIZE)
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('axes', labelsize=MEDIUM_SIZE)
plt.rc('xtick', labelsize=SMALL_SIZE)
plt.rc('ytick', labelsize=SMALL_SIZE)
plt.rc('figure', titlesize=LARGE_SIZE)


def check_custom_helper(original, new):
    if 'custom' in original:
        new['custom'] = original['custom']


def reduce_helper(acc, val):
    acc.extend(val['values'])
    return acc


def save_figure(path_to_figure, fig):
    fig.savefig(path_to_figure, bbox_inches='tight', pad_inches=0.2)


def generate_single_graph(values: [{}], create_fig: bool = True, x_axis=None, title='', ylabel='', xlabel='',
                          legends=None, grid=False, ylim=None, use_percentage=False):
    if x_axis is None:
        x_axis = np.arange(1, len(values[0]['values']) + 1)
    values_to_plot = []
    for value in values:
        values_to_plot.append(x_axis)
        if use_percentage:
            values_to_add = [result * 100 for result in value['values']]
        else:
            values_to_add = value['values']
        values_to_plot.append(values_to_add)
        if 'custom' in value:
            values_to_plot.append(value['custom'])
    if create_fig:
        fig = plt.figure(figsize=(20.48, 15.36))
    plt.plot(*values_to_plot)
    if ylim:
        plt.ylim(ylim)
    plt.grid(grid)
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    if legends:
        plt.legend(legends)
    if create_fig:
        return fig


def single_plot(values: [{}], x_axis=None, title='', ylabel='', xlabel='', legends=None, grid=False, ylim=None,
                use_percentage: bool = True):
    return generate_single_graph(values=values, create_fig=True, x_axis=x_axis, title=title, ylabel=ylabel,
                                 xlabel=xlabel, legends=legends, grid=grid, ylim=ylim, use_percentage=use_percentage)


def single_plot_zoomed_in(values: [{}], x_axis=None, title='', ylabel='', xlabel='', legends=None, grid=False,
                          ylim=None, use_percentage: bool = True):
    temp_value = values[0]['values']
    starting_index = int(max((0.25 * len(temp_value)) - 1, 0))
    x_axis = np.arange(starting_index + 1, len(temp_value) + 1)
    new_values = list(map(lambda value: {'values': value['values'][starting_index:]}, values))
    all_values = reduce(reduce_helper, new_values, [])
    max_value = max(all_values)
    mean, std = np.mean(all_values), np.std(all_values)
    ylim = (mean - 2.5 * std, max_value + 0.5 * std)
    if use_percentage:
        ylim = ylim[0] * 100, ylim[1] * 100
    fig = single_plot(values=new_values, x_axis=x_axis, title=title, ylabel=ylabel, xlabel=xlabel, legends=legends,
                      grid=grid, ylim=ylim, use_percentage=use_percentage)

    return fig


def difference_helper(x1, x2):
    # value = x1 - x2 if x1 >= 1.0 else x1 / x2
    return x1 - x2


def single_plot_difference_zoomed_in(
        values: [{}],
        x_axis=None,
        title='',
        ylabel='',
        xlabel='',
        legends=None,
        grid=False,
        ylim=None,
        use_percentage: bool = True):
    temp_value = values[0]['values']
    starting_index = int(max((0.25 * len(temp_value)) - 1, 0))
    x_axis = np.arange(starting_index + 1, len(temp_value) + 1)
    new_values = list(map(lambda value: {'values': value['values'][starting_index:]}, values))
    return single_plot_difference(
        values=new_values,
        x_axis=x_axis,
        title=title,
        ylabel=ylabel,
        xlabel=xlabel,
        legends=legends,
        grid=grid,
        ylim=ylim,
        use_percentage=use_percentage
    )


def single_plot_difference(
        values: [{}],
        x_axis=None,
        title='',
        ylabel='',
        xlabel='',
        legends=None,
        grid=False,
        ylim=None,
        use_percentage: bool = True):
    differences_values = [{'values': [0] * len(values[0]['values'])}]
    check_custom_helper(values[0], differences_values[0])
    for index in range(1, len(values)):
        difference = [difference_helper(x1, x2) for (x1, x2) in
                      zip(values[index]['values'], values[0]['values'])]
        new_dict = {'values': difference}
        check_custom_helper(values[index], new_dict)
        differences_values.append(new_dict)
    return single_plot(values=differences_values, x_axis=x_axis, title=title, ylabel=ylabel, xlabel=xlabel,
                       legends=legends, grid=grid, ylim=ylim, use_percentage=use_percentage)


def close_figure(figure):
    plt.close(figure)
