import os
from typing import Tuple, Any

import tensorflow as tf

import keras
from keras import optimizers, losses
from keras.callbacks import ModelCheckpoint, Callback
from keras.models import Sequential
from keras.backend.tensorflow_backend import set_session
from numpy.core.multiarray import ndarray

from src.callbacks import Time, NumericInfo, Evaluation
from src.layers import Dense, Layer
from src.dataset_description import get_dataset
from src.network_helper import NetworkHelper
from src.debug import DEBUG

TYPE_INFO = {
    'uint8': {'max': 255.0}
}


class Network(object):
    MODEL_DESCRIPTION = 'model_description.json'
    MODEL_SUMMARY = 'model_summary.txt'
    FORMAT_MODEL = 'keras_model_{}.h5'
    NAMED_FORMAT = 'keras_model_'
    FORMAT_LAYER = 'keras_model_{}_layer_{}_{}'
    FORMAT_RESULTS = 'keras_model_{}_results.json'
    DEFAULT_METRIC = ['accuracy']

    @staticmethod
    def default_output_layer(num_classes: int, activation: str = 'softmax', requiresFlatten: bool = False):
        return Dense(num_units=num_classes, activation=activation, requires_flatten=requiresFlatten)

    def __init__(self, dataset: str, name: str = None, batch_size: int = 32, epochs: int = 100,
                 checkpoint: int = 25, save_folder: str = 'saved_models', description=None,
                 measure: bool = False, add_batch_size_to_name: bool = True) -> None:
        if not isinstance(dataset, str):
            raise TypeError('Dataset deve ser uma string')
        self.__real_dataset = get_dataset(dataset)
        if self.__real_dataset is None:
            raise KeyError(f'Dataset {dataset} não existe')
        if description is None:
            description = {'name': 'Project', 'layers': []}
        if add_batch_size_to_name:
            save_dir = '{0}_{1}'.format(dataset if name is None else name, batch_size)
        else:
            save_dir = '{0}'.format(dataset if name is None else name)
        self.__helper = NetworkHelper(save_dir, save_folder=save_folder)
        self.__num_classes = self.__real_dataset['num_classes']
        self.__batch_size = batch_size
        self.__epochs = epochs
        self.__checkpoint = checkpoint
        self.__model = Sequential()
        self.__measure = measure
        self.__description = description
        self.__description['dataset'] = dataset
        self.__description['folder_name'] = name
        self.__description['batch_size'] = batch_size
        self.__description['epochs'] = epochs
        self.__description['checkpoint'] = checkpoint
        self.__description['save_folder'] = save_folder
        self.__layers = []

    @staticmethod
    def load_model(model_path):
        actual_path = model_path
        if not os.path.isabs(model_path):
            actual_path = os.path.join(os.getcwd(), model_path)
        (helper, model, dataset_name) = NetworkHelper.load_model(actual_path)
        project = Network(dataset_name)
        project.__helper = helper
        project.model = model
        project.__real_dataset = get_dataset(dataset_name)
        return project

    @property
    def project_folder(self):
        return self.__helper.save_dir

    @property
    def description(self):
        return self.__description

    @property
    def dataset(self):
        return self.__real_dataset.get('set')

    @property
    def dataset_train_shape(self):
        if self.__real_dataset.get('greyscale', False):
            return self.dataset.load_data()[0][0].shape + (1,)
        return self.dataset.load_data()[0][0].shape

    @property
    def dataset_test_shape(self):
        if self.__real_dataset.get('greyscale', False):
            return self.dataset.load_data()[1][0].shape + (1,)
        return self.dataset.load_data()[1][0].shape

    @property
    def num_classes(self):
        return self.__real_dataset['num_classes']

    @property
    def batch_size(self):
        return self.__batch_size

    @property
    def epochs(self):
        return self.__epochs

    @property
    def model_layers(self):
        return self.model.layers

    @property
    def model(self):
        return self.__model

    @model.setter
    def model(self, model):
        self.__model = model

    def add_layer(self, layer: Layer):
        self.__layers.append(layer)
        for keras_layer in layer.keras_layers:
            self.model.add(keras_layer)

    def change_dataset(self, new_dataset: str) -> bool:
        new_dataset = get_dataset(new_dataset)
        if new_dataset is not None:
            self.__real_dataset = new_dataset
            self.__helper = NetworkHelper(new_dataset)
            return True
        return False

    def __add_keras_layers(self):
        for model_layer in self.__layers:
            if self.__measure:
                model_layer.active_measure()
            for keras_layer in model_layer.keras_layers:
                self.model.add(keras_layer)

    def add_output_layer(self, activation: str = 'softmax', requiresFlatten: bool = False) -> None:
        """
        Equivalente a adicionar a adicionar uma camada densa com o número de unidades igual ao número de classes do
        dataset e activação igual à função especificada, por omissão a função de activação é a softmax.
        :rtype: None
        """
        self.add_layer(
            Network.default_output_layer(self.num_classes, activation=activation,
                                         requiresFlatten=requiresFlatten))

    def _modify_dataset(self, data: Tuple[Tuple[ndarray, ndarray], Tuple[Any, ndarray]]) -> \
            Tuple[Tuple[ndarray, ndarray], Tuple[Any, ndarray]]:
        (x_train, y_train), (x_test, y_test) = data
        if self.__real_dataset.get('greyscale', False):
            x_train = x_train.reshape(self.dataset_train_shape)
            x_test = x_test.reshape(self.dataset_test_shape)
        y_train = keras.utils.to_categorical(y_train, self.num_classes)
        y_test = keras.utils.to_categorical(y_test, self.num_classes)
        type_info = TYPE_INFO.get(x_test[0].dtype.name)
        if type_info is not None:
            x_train = x_train.astype('float32')
            x_test = x_test.astype('float32')
            x_train /= type_info['max']
            x_test /= type_info['max']
        return (x_train, y_train), (x_test, y_test)

    def __evaluate(self, x_test, y_test) -> (float, float):
        return self.model.evaluate(x_test, y_test, batch_size=self.batch_size, verbose=1 if DEBUG == 1 else 0)

    def fit(self, starting_epoch: int = 0, loss=losses.categorical_crossentropy,
            optimizer=optimizers.rmsprop(lr=0.0001, decay=1e-6), metrics=None, callbacks=None):
        if callbacks is None:
            callbacks = []
        if metrics is None:
            metrics = Network.DEFAULT_METRIC
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        (x_train, y_train), (x_test, y_test) = self._modify_dataset(self.dataset.load_data())
        all_callbacks = []
        all_callbacks.extend(self.__create_callbacks('model_{epoch}.h5', (x_test, y_test)))
        all_callbacks.extend(callbacks)
        lines_descriptions = []
        self.model.summary(print_fn=lambda s: lines_descriptions.append(s))
        self.__helper.save_summary(Network.MODEL_SUMMARY, '\n'.join(lines_descriptions))
        self.model.fit(x_train, y_train, batch_size=self.batch_size, initial_epoch=starting_epoch,
                       epochs=self.epochs, validation_split=0.1,
                       validation_data=(x_test, y_test), shuffle=True, verbose=2 if DEBUG == 1 else 0,
                       callbacks=all_callbacks)
        self.__description['layers'] = [model_layer.report for model_layer in self.__layers]
        self.__helper.save_description(Network.MODEL_DESCRIPTION, self.description)

    def __create_callbacks(self, model_format: str, test_data: Tuple[Any, Any]) -> [Callback]:
        return [
            ModelCheckpoint(os.path.join(self.__helper.save_dir, model_format), monitor='val_acc',
                            period=self.__checkpoint, verbose=1 if DEBUG == 1 else 0),
            Time(self.__helper.save_dir),
            NumericInfo(self.__helper.save_dir, self.__checkpoint),
            Evaluation(self.model, test_data, self.__helper.save_dir, self.batch_size)
        ]

    def evaluate(self) -> (float, float):
        (_, _), (x_test, y_test) = self._modify_dataset(self.dataset.load_data())
        return self.__evaluate(x_test=x_test, y_test=y_test)
