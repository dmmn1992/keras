import os
import re
from typing import Union, Type

from keras import models

from src.network import Network

regex = re.compile(r"model_([0-9]{1,5}).*", re.IGNORECASE)


class Transferable(object):
    def __init__(self, arch: Union[Type[Network], Network], dataset: str, model_path: str,
                 trainable_layers: bool = False, batch_size: int = 32,
                 num_layers: int = 1, save_folder: str = 'save_models') -> None:
        actual_path = model_path
        if not os.path.isabs(model_path):
            actual_path = os.path.join(os.getcwd(), model_path)
        self.network = arch
        self.trained_model = models.load_model(actual_path)
        self.transferred_name = os.path.dirname(model_path).split('/')[-1]
        self.model_epoch_value = regex.search(os.path.basename(model_path)).group(1)
        self.dataset = dataset
        self.trainable_layers = trainable_layers
        self.batch_size = batch_size
        self.save_folder = save_folder
        self.num_layers = num_layers

    @property
    def model(self):
        return self.trained_model

    def fit(self):
        """

        :return:
        """
        """
            Copiamos logo os valores para que não ocorram problemas com a sessão do Tensorflow
        """
        layers_weights_biases = [(idx, a.get_weights()) for (idx, a) in enumerate(self.trained_model.layers[:-2]) if
                                 a.get_weights() and not a.name.startswith('batch_normalization')]
        self.__evaluate_transferred(layers_weights_biases[:self.num_layers])

    def __evaluate_transferred(self, transferable_layers: [(int, ([float], [float]))]) -> None:
        is_learning = '' if self.trainable_layers else '_not'
        project = self.network(name=self.dataset,
                               batch_size=self.batch_size,
                               save_folder=self.save_folder,
                               add_batch_size_to_name=False,
                               folder_name=Transferable.format_destination_folder_name(
                                len(transferable_layers),
                                is_learning,
                                int(self.model_epoch_value)
                            ))
        layers = project.model_layers
        for (index, weights_biases) in transferable_layers:
            self.__convert_layer(layers[index], list(weights_biases), trainable=self.trainable_layers)
        max_layer_index, _ = transferable_layers[-1]
        for layer in layers[:max_layer_index]:
            layer.trainable = self.trainable_layers
        project.fit()

    @staticmethod
    def format_destination_folder_name(num_transferred_layers: int, are_learning: str, epoch: int):
        return f'epoch_{epoch}-{num_transferred_layers}_layers{are_learning}_learning'

    @staticmethod
    def __convert_layer(layer, weights_biases, trainable=False):
        layer.set_weights(weights_biases)
        layer.trainable = trainable
        layer.name = f'{"trainable" if trainable else "not_trainable"}_{layer.name}'
