import os
from typing import Any

import numpy as np
import json
from keras import models


class NetworkHelper(object):
    """

    """

    def __init__(self, dataset_name: str, save_folder: str = 'saved_models') -> None:
        """

        :param dataset_name: Nome do dataset
        """
        self.__dataset_name = dataset_name
        self.__save_dir = os.path.join(os.getcwd(), save_folder, dataset_name)
        if not os.path.isdir(self.__save_dir):
            os.makedirs(self.__save_dir)

    @property
    def dataset_name(self):
        return self.__dataset_name

    @property
    def save_dir(self):
        return self.__save_dir

    def save_description(self, file_name: str, data):
        with open(os.path.join(self.save_dir, file_name), 'w') as fp:
            json.dump(data, fp)

    def save_summary(self, file_name: str, data: str):
        with open(os.path.join(self.save_dir, file_name), 'w') as fp:
            fp.write(data)

    def save_model(self, format_name: str, format_layer: str, model: models.Model, epochs: int) -> None:
        """

        :param format_layer:
        :param format_name:
        :param model:
        :param epochs:
        """
        model.save(os.path.join(self.__save_dir, format_name.format(epochs)))
        interesting_layers = model.layers[:-2]
        for i, layer in enumerate(interesting_layers):
            for val_index, val in enumerate(layer.get_weights()):
                name = format_layer.format(epochs, i, self.__find_layer_element_type(val_index))
                np.save(os.path.join(self.__save_dir, name), val)

    def save_model_results(self, fit_values, evaluation_result, epoch_timmings: [float], format_name: str, epochs: int):
        data_file = {
            'fit': {},
            'epochs_timming': epoch_timmings,
            'evaluation': {'loss': evaluation_result[0], 'accuracy': evaluation_result[1]}
        }
        for key in fit_values.history:
            (mean, max_val, min_val, median) = NetworkHelper.__calculate_information(fit_values.history[key])
            data_file['fit'][key] = {'mean': mean, 'max': max_val, 'min': min_val, 'median': median}
        with open(os.path.join(self.__save_dir, format_name.format(epochs)), 'w') as fp:
            json.dump(data_file, fp)

    @staticmethod
    def __calculate_information(information):
        mean = np.mean(information)
        max_val = np.amax(information)
        min_val = np.amin(information)
        median = np.median(information)
        return mean, max_val, min_val, median

    @staticmethod
    def __find_layer_element_type(index):
        return 'weights' if index == 0 else 'biases'

    @staticmethod
    def load_model(model_path: os.path) -> ('NetworkHelper', models.Model):
        dataset_name = os.path.dirname(model_path).split('/')[-1]
        try:
            # noinspection PyTypeChecker
            return NetworkHelper(dataset_name), models.load_model(model_path), dataset_name
        except OSError:
            return None, None
        except ValueError:
            return None, None

    def save_evaluation(self, values, param: str):
        model_path = os.path.join(self.save_dir, param.format())
