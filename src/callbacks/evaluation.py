import json
from typing import Tuple, Union

from keras.callbacks import Callback
from keras.models import Model
from os.path import join


class Evaluation(Callback):
    def __init__(self, model: Model, test_data: Tuple[list, list], save_dir: str, batch_size: int = 32) -> None:
        super(Evaluation, self).__init__()
        self.__model = model
        self.__x_test = test_data[0]
        self.__y_test = test_data[1]
        self.__save_dir = save_dir
        self.__values = []
        self.__batch_size = batch_size

    def on_epoch_end(self, epoch, logs=None):
        values = self.model.evaluate(self.__x_test, self.__y_test, batch_size=self.__batch_size, verbose=0)
        self.__values.append(values)

    def on_train_end(self, logs=None):
        result = {}
        for idx, name in enumerate(self.__model.metrics_names):
            result[name] = list(map(lambda x: x[idx], self.__values))
        self.__write_to_file(result)

    def __write_to_file(self, data: dict) -> None:
        with open(join(self.__save_dir, f'model_evaluation.json'), 'w') as fp:
            json.dump(data, fp)
