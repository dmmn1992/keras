import json
from os.path import join
from time import time
from keras.callbacks import Callback


class Time(Callback):
    def __init__(self, save_dir):
        super(Time, self).__init__()
        self.__times = []
        self.__save_dir = save_dir
        self.__current_start = None

    @property
    def times(self):
        return self.__times

    def on_epoch_begin(self, epoch, logs=None):
        self.__current_start = time()

    def on_epoch_end(self, epoch, logs=None):
        self.__times.append(time() - self.__current_start)

    def on_train_end(self, logs=None):
        self.__save_to_file({'times': self.__times})

    def __save_to_file(self, data):
        with open(join(self.__save_dir, f'model_time_info.json'), 'w') as fp:
            json.dump(data, fp)
