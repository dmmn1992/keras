import json
from keras.callbacks import Callback
from os.path import join
from numpy import mean, median, amax, amin


class NumericInfo(Callback):
    def __init__(self, save_dir: str, period: int = 25) -> None:
        super(NumericInfo, self).__init__()
        self.__save_dir = save_dir
        self.__period = period
        self.__runs_info = []
        self.__epochs_since_period = 0

    def on_epoch_end(self, epoch, logs=None):
        if logs is not None:
            self.__runs_info.append(logs)
        self.__epochs_since_period += 1
        if self.__epochs_since_period >= self.__period:
            self.__epochs_since_period = 0
            result = {}
            for key in [*logs]:
                result[key] = NumericInfo.__write_information_for_key(self.__runs_info)
            self.__write_to_file(epoch + 1, result)

    @staticmethod
    def __write_information_for_key(key: str, runs: []) -> dict:
        all_values = list(map(lambda x: x[key], runs))
        return {
            'all_values': all_values,
            'mean': mean(all_values),
            'max': amax(all_values),
            'min': amin(all_values),
            'median': median(all_values)
        }

    def __write_to_file(self, epoch: int, data: dict) -> None:
        with open(join(self.__save_dir, f'model_{epoch}_numeric_info.json'), 'w') as fp:
            json.dump(data, fp)
