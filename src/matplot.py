import numpy as np
import matplotlib.pyplot as plt
import json


def extract_all_values(data: dict, key='all_values') -> [float]:
    return np.array(data[key]) * 100


def load_json(path: str) -> dict:
    with open(path, 'r') as fp:
        return json.load(fp)


def plot_val_acc(values: [], x_axis=np.arange(0, 100)):
    fig = plt.figure(figsize=(20.48, 15.36))
    plt.subplot(3, 1, 1)
    plt.plot(x_axis, values[0], x_axis, values[1], x_axis, values[2], x_axis, values[3])
    plt.grid(True)
    plt.legend(['Original', '1 Camada Transferida', '2 Camada Transferida', '3 Camada Transferida'])
    plt.ylabel('Precisão Conjunto Teste (%)')
    plt.xlabel('Epoch')
    plt.subplot(3, 1, 2)
    plt.plot(x_axis, values[0], x_axis, values[1], x_axis, values[2], x_axis, values[3])
    plt.grid(True)
    plt.ylim(85, 91)
    plt.legend(['Original', '1 Camada Transferida', '2 Camada Transferida', '3 Camada Transferida'])
    plt.ylabel('Precisão Conjunto Teste (%)')
    plt.xlabel('Epoch')
    plt.subplot(3, 1, 3)
    plt.plot(x_axis, abs(values[1] - values[0]), 'tab:orange', x_axis, abs(values[2] - values[0]), 'tab:green', x_axis,
             abs(values[3] - values[0]), 'tab:red')
    plt.grid(True)
    plt.legend(['1 Camada Transferida', '2 Camada Transferida', '3 Camada Transferida'])
    plt.ylabel('Diferença (%)')
    plt.xlabel('Epoch')
    fig.savefig('./fashion_mnist_acc.png', bbox_inches='tight', pad_inches=0.2)


def plot_training_per_epoch(values: [], x_axis=np.arange(0, 100)):
    fig = plt.figure(figsize=(20.48, 15.36))
    plt.subplot(2, 1, 1)
    plt.plot(x_axis, values[0], x_axis, values[1], x_axis, values[2], x_axis, values[3])
    plt.grid(True)
    plt.legend(['Original', '1 Camada Transferida', '2 Camada Transferida', '3 Camada Transferida'])
    plt.ylabel('Tempo de treino (s)')
    plt.xlabel('Epoch')
    plt.subplot(2, 1, 2)
    plt.plot(x_axis, np.repeat(100, 100), x_axis, (values[1] / values[0]) * 100, x_axis, (values[2] / values[0]) * 100,
             x_axis,
             (values[3] / values[0]) * 100)
    plt.grid(True)
    plt.legend(['Original', '1 Camada Transferida', '2 Camada Transferida', '3 Camada Transferida'])
    plt.ylabel('Tempo de treino relativo ao original (%)')
    plt.xlabel('Epoch')
    fig.savefig('./fashion_mnist_time.png', bbox_inches='tight', pad_inches=0.2)


fashion_mnist = load_json('./saved_models/fashion_mnist/model_100_numeric_info.json')
fashion_mnist_1 = load_json('./saved_models/mnist_to_fashion_mnist_epoch_25_1_not_learning/model_100_numeric_info.json')
fashion_mnist_2 = load_json('./saved_models/mnist_to_fashion_mnist_epoch_25_2_not_learning/model_100_numeric_info.json')
fashion_mnist_3 = load_json('./saved_models/mnist_to_fashion_mnist_epoch_25_3_not_learning/model_100_numeric_info.json')

fashion_mnist_timming = load_json('./saved_models/fashion_mnist/model_time_info.json')
fashion_mnist_1_timming = load_json(
    './saved_models/mnist_to_fashion_mnist_epoch_25_1_not_learning/model_time_info.json')
fashion_mnist_2_timming = load_json(
    './saved_models/mnist_to_fashion_mnist_epoch_25_2_not_learning/model_time_info.json')
fashion_mnist_3_timming = load_json(
    './saved_models/mnist_to_fashion_mnist_epoch_25_3_not_learning/model_time_info.json')

files = [fashion_mnist, fashion_mnist_1, fashion_mnist_2, fashion_mnist_3]

time_files = [fashion_mnist_timming, fashion_mnist_1_timming, fashion_mnist_2_timming, fashion_mnist_3_timming]

all_val_acc = list(map(extract_all_values, map(lambda x: x['val_acc'], files)))

time_all = list(map(lambda x: np.array(x['times']), time_files))

plot_val_acc(all_val_acc)

# plot_training_per_epoch(time_all)
