import os
import pickle
import re
import fnmatch
import numpy as np
from PIL import Image
from keras.datasets import cifar10

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

pattern = re.compile("bw_[a-zA-Z_ ]+_([0-9]{1,6})_[0-9]+\\.png")

cwd = os.getcwd()
folderDir = os.path.join(cwd, "temp_grayscale")
os.makedirs(folderDir, mode=0o755, exist_ok=True)

new_data_set_folder = os.path.join(cwd, "bw_cifar10")


def save_set(x, y, fixed_path, set_name):
    for idx in range(x.shape[0]):
        Image.fromarray(x[idx]).save(os.path.join(fixed_path, f"{set_name}_{idx}_{y[idx][0]}.png"))


def load_bw_set(fixed_path, set_name, shape):
    data_set = np.empty(shape, dtype='uint8')
    for root, dirs, files in os.walk(fixed_path):
        for validName in fnmatch.filter(files, f"bw_{set_name}*"):
            index = int(pattern.search(validName).group(1))
            img = Image.open(os.path.join(fixed_path, validName))
            img.load()
            img = img.convert('RGB')
            data_set[index] = np.asarray(img, dtype='uint8')
    return data_set


def save_bw_set(data_set, labels, file_name):
    data = {'data': data_set, 'labels': labels}
    with open(file_name, "wb") as open_file:
        pickle.dump(data, open_file)


if __name__ == u"__main__":
    # save_set(x_train, y_train, folderDir, "x_train")
    # save_set(x_test, y_test, folderDir, "x_test")
    train = load_bw_set(folderDir, "x_train", x_train.shape)
    save_bw_set(train, y_train, os.path.join(new_data_set_folder, "train"))
    test = load_bw_set(folderDir, "x_test", x_test.shape)
    save_bw_set(test, y_test, os.path.join(new_data_set_folder, "test"))
    exit(0)
