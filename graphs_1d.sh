#!/usr/bin/env bash

if [ "$HAS_SOURCE_CONFIGURE" != "1" ]; then
  source configure
fi

sleep_time="30"

function getMap () {
    local map="$1"
    local key="$2"
    if [[ $BASH_VERSION == 3* ]]; then
      local variable="${map}_${key}"
    else
      local variable="$map[$key]"
    fi
    echo "${!variable}"
}

if [ ! -z "$BASH_VERSION" ]; then
  if [[ $BASH_VERSION == 3* ]]; then
    transferable_dataset_keys=("mnist" "fashion_mnist")
    transferable_dataset_values=("fashion_mnist" "mnist")
    for (( i = 0; i < ${#transferable_dataset_keys[@]}; i++ )); do
      declare "transferable_dataset_${transferable_dataset_keys[i]}=${transferable_dataset_values[i]}"
    done
    keys=( "${transferable_dataset_keys[@]}" )
    transferable_layers_keys=("simple" "stride5" "deep" "regular_simple" "regular_stride5" "regular_deep")
    transferable_layers_values=("3" "3" "5" "3" "3" "5")
    for (( i = 0; i < ${#transferable_layers_keys[@]}; i++ )); do
      declare "transferable_dataset_${transferable_layers_keys[i]}=${transferable_layers_values[i]}"
    done
  else
    declare -A transferable_dataset=(["mnist"]="fashion_mnist" ["fashion_mnist"]="mnist")
    declare -A transferable_layers=(["simple"]="3" ["stride5"]="3" ["deep"]="5" ["regular_simple"]="3"  ["regular_stride5"]="3" ["regular_deep"]="5")
    keys="${!transferable_dataset[@]}"
  fi
fi

declare -a archs=("simple" "stride5" "deep" "regular_simple" "regular_stride5" "regular_deep")
declare -a learnings=("yes" "no")

folder_start_name="${1:-$SPECIFIC_FOLDER_NAME}"

original_save_folder="${folder_start_name}_results"

transferred_save_folder="${folder_start_name}_transfer_results"

graphs_save_folder="${folder_start_name}_graphs"

for initial_dataset in "${keys[@]}"; do
  transferred_dataset=$(getMap "transferable_dataset" "$initial_dataset")
  for arch in "${archs[@]}"; do
    for learning in "${learnings[@]}"; do
      echo -e "Dataset: ${initial_dataset}\t\tTransferred to: ${transferred_dataset}\tArchitecture: ${arch}\tLearning: ${learning}"
      #echo -e "Arch: ${arch}\nOriginal Dataset: ${initial_dataset}\nDataset: ${transferred_dataset}\nResults Original Folder: ${original_save_folder}\nResults Transferred Folder: ${transferred_save_folder}\nLearning: ${learning}\nSave Folder: ${graphs_save_folder}"
      MPLBACKEND="agg" python src/cmds/accuracy_graph_1d.py -arch "$arch" -originalDataset "$initial_dataset" -dataset "$transferred_dataset" -resultsOriginalFolder "$original_save_folder" -resultsTransferredFolder "$transferred_save_folder" -learning "$learning" -saveFolder "$graphs_save_folder" -yLabel "Taxa de acerto (%)" -xLabel "Epochs" -title "Transferência de ${initial_dataset} para ${transferred_dataset}" -grid "yes" -usePercentage "yes"
      MPLBACKEND="agg" python src/cmds/timing_graph_1d.py -arch "$arch" -originalDataset "$initial_dataset" -dataset "$transferred_dataset" -resultsOriginalFolder "$original_save_folder" -resultsTransferredFolder "$transferred_save_folder" -learning "$learning" -saveFolder "$graphs_save_folder" -yLabel "Tempo (s)" -xLabel "Epochs" -title "Transferência de ${initial_dataset} para ${transferred_dataset}" -grid "yes" -usePercentage "no"
    done
  done
done

config_deactivate