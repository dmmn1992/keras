#!/bin/bash

if [ "$HAS_SOURCE_CONFIGURE" != "1" ]; then
  source configure
fi

sleep_time="120"

function sendToSlack () {
        echo -e "sendToSlack received: $1"
        curl -X POST -H "Content-type: application/json" --data "{\"text\":\"$1\"}" "https://hooks.slack.com/services/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"
        echo -e ""
}

declare -a archs=("good" "best")
declare -a datasets=("cifar10" "cifar100")

for arch in "${archs[@]}"; do
    for dataset in "${datasets[@]}"; do
            echo -e "A processar a arquitetura ${arch}, para o ${dataset} com batch ${1:-128}"
            sendToSlack "$(date +'%d/%m/%y %H:%M:%S'): A processar a arquitetura ${arch}, para o ${dataset} com batch ${1:-128}"
        python src/cmds/run_2d_test.py -arch "$arch"  -dataset "$dataset" -folderName "${arch}_${dataset}_jetson" -batchSize "${1:-128}" -saveFolder "${SPECIFIC_FOLDER_NAME}_results"
        echo -e "Terminei a arquitetura ${arch}, para o ${dataset} com batch ${1:-128}"
        sendToSlack "$(date +'%d/%m/%y %H:%M:%S'): Terminei a arquitetura ${arch}, para o ${dataset} com batch ${1:-128}"
    done
    echo -e "A descansar ${sleep_time}s..."
    sleep "$sleep_time"
done


config_deactivate