#!/bin/bash

if [ "$HAS_SOURCE_CONFIGURE" != "1" ]; then
  source configure
fi

sleep_time="5"

declare -a archs=("simple" "stride5" "deep" "regular_simple" "regular_stride5" "regular_deep")
declare -a datasets=("mnist" "fashion_mnist")

function sendToSlack () {
        echo -e "sendToSlack received: $1"
        curl -X POST -H "Content-type: application/json" --data "{\"text\":\"$1\"}" "https://hooks.slack.com/services/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"
        echo -e ""
}

for arch in "${archs[@]}"
do
    for dataset in "${datasets[@]}"
    do
        echo -e "Running ${arch} ${dataset} batch size ${1:-32}"
        sendToSlack "Started running ${arch} with dataset ${dataset} and batch size = ${1:-32} at $(date +'%d/%m/%y %H:%M:%S')"
        python src/cmds/run_1d_test.py -arch "$arch" -dataset "$dataset" -folderName "${arch}_${dataset}" -batchSize "${1:-32}" -saveFolder "${SPECIFIC_FOLDER_NAME}_results"
        echo -e "Finished running ${arch} ${dataset} batch size ${1:-32}"
        sendToSlack "Finished running ${arch} with dataset ${dataset} and batch size = ${1:-32} at $(date +'%d/%m/%y %H:%M:%S')"
        done
        sendToSlack "Run1DTest: terminei a arquitetura ${arch} para todos os datasets com batchSize = ${1:-32}"
    echo -e "A descansar ${sleep_time}s..."
    sleep "$sleep_time"
done


config_deactivate
