#!/usr/bin/env bash

if [ "$HAS_SOURCE_CONFIGURE" != "1" ]; then
  source configure
fi

sleep_time="1"

function getMap () {
    local map="$1"
    local key="$2"
    if [[ $BASH_VERSION == 3* ]]; then
      local variable="${map}_${key}"
      echo "${!variable}"
    else
	    local variable="$map[$key]"
      echo "${!variable}"
    fi
}

function sendToSlack () {
        echo -e "sendToSlack received: $1"
        curl -X POST -H "Content-type: application/json" --data "{\"text\":\"$1\"}" "https://hooks.slack.com/services/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"
        echo -e ""
}

function transfer () {
        local arch="$1"
        local new_dataset="$2"
        local model_path="$3"
        local dest_folder="$4"
        local batch_size="$5"
        local number_layers="$6"
        local all_learn="$7"
        sendToSlack "$(date +'%d/%m/%y %H:%M:%S'): A começar a transferência da arquitetura ${arch}, com ${number_layers} camadas, com aprendizagem: ${all_learn}"
        python src/cmds/transfer_1d.py -arch "$arch" -dataset "$new_dataset" -modelPath "$model_path" -transferedLearn "$all_learn" -numberLayers "$number_layers" -batchSize "${batch_size}" -saveFolder "$dest_folder"
        sendToSlack "$(date +'%d/%m/%y %H:%M:%S'): Transferência terminada da arquitetura ${arch}, com ${number_layers} camadas, com aprendizagem: ${all_learn}"
}

if [ ! -z "$BASH_VERSION" ]; then
  if [[ $BASH_VERSION == 3* ]]; then
    transferable_dataset_keys=("mnist" "fashion_mnist")
    transferable_dataset_values=("fashion_mnist" "mnist")
    for (( i = 0; i < ${#transferable_dataset_keys[@]}; i++ )); do
      declare "transferable_dataset_${transferable_dataset_keys[i]}=${transferable_dataset_values[i]}"
    done
    keys=( "${transferable_dataset_keys[@]}" )
    transferable_layers_keys=("simple" "stride5" "deep" "regular_simple" "regular_stride5" "regular_deep")
    transferable_layers_values=("3" "3" "5" "3" "3" "5")
    for (( i = 0; i < ${#transferable_layers_keys[@]}; i++ )); do
      declare "transferable_layers_${transferable_layers_keys[i]}=${transferable_layers_values[i]}"
    done
  else
    declare -A transferable_dataset=(["mnist"]="fashion_mnist" ["fashion_mnist"]="mnist")
    declare -A transferable_layers=(["simple"]="3" ["stride5"]="3" ["deep"]="5" ["regular_simple"]="3"  ["regular_stride5"]="3" ["regular_deep"]="5")
    keys=( "${!transferable_dataset[@]}" )
  fi
fi

declare -a archs=("simple" "stride5" "deep" "regular_simple" "regular_stride5" "regular_deep")

if [ "$1" == "ALL"  ];then
        ALL=1
else
        if [ $# -lt 2 ];then
                echo -e "Deve indicar o número do batch e o epoch a analisar, caso pretenda analisar todos passe como primeiro argumento a string ALL"
                exit 1
        fi
        ALL=0
        CHOSEN_BATCH="$1"
        CHOSEN_EPOCH="$2"
fi

for initial_dataset in "${keys[@]}"; do
  transferred_dataset=$(getMap "transferable_dataset" "$initial_dataset")
  for arch in "${archs[@]}"; do
          if [ $ALL -eq 1  ];then
                batch_sizes="$(find "${SPECIFIC_FOLDER_NAME}_results" -name "${arch}_${initial_dataset}_*" | sed 's|^.*/.*_\([0-9]\{1,\}\)$|\1|g' | sort -n)"
          else
                batch_sizes="$CHOSEN_BATCH"
          fi
    for batch_size in ${batch_sizes}; do
      model_folder="${SPECIFIC_FOLDER_NAME}_results/${arch}_${initial_dataset}_${batch_size}/"
      if [ $ALL -eq 1 ];then
              epochs="$(find "${model_folder}" -name "model_*.h5" | sed 's|^.*/.*_\([0-9]\{1,\}\).h5$|\1|g' | sort -n)"
      else
                epochs="$CHOSEN_EPOCH"
      fi
      for epoch in ${epochs}; do
        model_path="${model_folder}model_${epoch}.h5"
        save_folder="${SPECIFIC_FOLDER_NAME}_transfer_results/${arch}/${initial_dataset}_to_${transferred_dataset}/batch_${batch_size}"
        num_max_layers=$(getMap "transferable_layers" "$arch")
        for ((number_layers = 1 ; number_layers <= num_max_layers ; number_layers++)); do
        echo -e "Arquitetura: ${arch}|Transferência: ${initial_dataset}->${transferred_dataset}|Caminho do modelo: ${model_path}|Número Camadas: ${number_layers}|Batch: ${batch_size}|Pasta destino: ${save_folder}"
                transfer "$arch" "$transferred_dataset" "$model_path" "$save_folder" "$batch_size" "$number_layers" "yes"
                transfer "$arch" "$transferred_dataset" "$model_path" "$save_folder" "$batch_size" "$number_layers" "no"
        done
        sendToSlack "$(date +'%d/%m/%y %H:%M:%S'): Transfer1D: terminei a arquitetura do dataset ${initial_dataset} para o dataset ${transferred_dataset} com batchSize = ${batch_size} e epoch = ${epoch}"
        echo -e "A descansar ${sleep_time}s..."
        sleep "$sleep_time"
      done
    done
  done
done

config_deactivate
