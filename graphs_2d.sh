#!/usr/bin/env bash

if [ "$HAS_SOURCE_CONFIGURE" != "1" ]; then
  source configure
fi

sleep_time="30"

function getMap () {
    local map="$1"
    local key="$2"
    if [[ $BASH_VERSION == 3* ]]; then
      local variable="${map}_${key}"
    else
      local variable="$map[$key]"
    fi
    echo "${!variable}"
}

if [ ! -z "$BASH_VERSION" ]; then
  if [[ $BASH_VERSION == 3* ]]; then
    transferable_dataset_keys=("cifar10" "cifar100")
    transferable_dataset_values=("cifar100" "cifar10")
    for (( i = 0; i < ${#transferable_dataset_keys[@]}; i++ )); do
      declare "transferable_dataset_${transferable_dataset_keys[i]}=${transferable_dataset_values[i]}"
    done
    keys=( "${transferable_dataset_keys[@]}" )
    transferable_layers_keys=("good" "best")
    transferable_layers_values=("3" "6")
    for (( i = 0; i < ${#transferable_layers_keys[@]}; i++ )); do
      declare "transferable_dataset_${transferable_layers_keys[i]}=${transferable_layers_values[i]}"
    done
  else
    declare -A transferable_dataset=(["cifar10"]="cifar100" ["cifar100"]="cifar10")
    declare -A transferable_layers=(["good"]="3" ["best"]="6")
    keys="${!transferable_dataset[@]}"
  fi
fi

declare -a archs=("conv_heavy" "regular_conv_heavy" "deep" "regular_deep")
declare -a learnings=("yes" "no")

folder_start_name="${1:-$SPECIFIC_FOLDER_NAME}"

original_save_folder="${folder_start_name}_results"

transferred_save_folder="${folder_start_name}_transfer_results"

graphs_save_folder="${folder_start_name}_graphs"

for initial_dataset in "${keys[@]}"; do
  transferred_dataset=$(getMap "transferable_dataset" "$initial_dataset")
  for arch in "${archs[@]}"; do
    for learning in "${learnings[@]}"; do
      echo -e "Dataset: ${initial_dataset}\tTransferred to: ${transferred_dataset}\tArchitecture: ${arch}\tLearning: ${learning}"
      MPLBACKEND="agg" python src/cmds/accuracy_graph_2d.py -arch "$arch" -originalDataset "$initial_dataset" -dataset "$transferred_dataset" -resultsOriginalFolder "$original_save_folder" -resultsTransferredFolder "$transferred_save_folder" -learning "$learning" -saveFolder "$graphs_save_folder" -yLabel "Taxa de acerto (%)" -xLabel "Epochs" -title "Transferência de ${initial_dataset} para ${transferred_dataset}" -grid "yes"
      MPLBACKEND="agg" python src/cmds/timing_graph_2d.py -arch "$arch" -originalDataset "$initial_dataset" -dataset "$transferred_dataset" -resultsOriginalFolder "$original_save_folder" -resultsTransferredFolder "$transferred_save_folder" -learning "$learning" -saveFolder "$graphs_save_folder" -yLabel "Tempo (s)" -xLabel "Epochs" -title "Transferência de ${initial_dataset} para ${transferred_dataset}" -grid "yes" -usePercentage "no"
    done
  done
done

config_deactivate